![Build Status](https://gitlab.com/claudio.sousa/connaitre-la-suisse/badges/master/build.svg)


---

This project generates a web page listing al the questions and answers used for the Swiss Naturalisation test.

## Demo:

Here is the [Demo](http://claudio.sousa.gitlab.io/connaitre-la-suisse/)

---

#### Original app 
Based on the [official](https://www.ge.ch/connaitre-la-suisse/quiz/story.html) app


